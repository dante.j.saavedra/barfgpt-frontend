import { environment } from "environments/environment.development";


export const newConversationsByUsername = async(username: string) => {
    try {
        const resp = await fetch(`${ environment.barfRag }/newConversation?username=${ username }`,{
            method: 'GET',
            
        });
        return resp.text();

    } catch (error) {
        throw new Error('Error al crear una nueva conversacion');
    }
}
