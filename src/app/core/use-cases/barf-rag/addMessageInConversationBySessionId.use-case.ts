import { environment } from "environments/environment.development";


export const addMessageInConversationBySessionId = async(sessionId: string, question: string) => {
    try {
        const resp = await fetch(`${ environment.barfRag }/conversacionwithRetrievalChain?sessionId=${ sessionId }&question=${ question }`,{
            method: 'GET',
            
        });
        return resp.text();

    } catch (error) {
        throw new Error('Error al añadir un mensaje en la conversacion');
    }
}
