import { ConversationsId } from "@interfaces/conversationsId.interface";
import { environment } from "environments/environment.development";


export const getConversationsByUsername = async(username: string) => {
    try {
        const resp = await fetch(`${ environment.barfRag }/getConversationsIdByUsername?username=${ username }`,{
            method: 'GET',
            
        });

        const conversations = await resp.json() as ConversationsId[];

        return conversations;

    } catch (error) {
        throw new Error('Error al obtener las conversaciones');
    }
}



