import { Conversation } from "@interfaces/interfaces";
import { environment } from "environments/environment.development";


export const getMessagesBySessionId = async(sessionId: string) => {
    try {
        const resp = await fetch(`${ environment.barfRag }/getAllMessages?sessionId=${ sessionId }`,{
            method: 'GET',
            
        });

        let conversations = await resp.json() as Conversation[];
        //quitar primer elemento
        conversations.shift();
        return conversations;

    } catch (error) {
        throw new Error('Error al obtener las conversaciones');
    }
}



