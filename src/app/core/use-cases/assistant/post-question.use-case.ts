import { QuestionResponse } from "@interfaces/questionResponse.interface";
import { environment } from "environments/environment.development";

export const postQuestionUseCase = async(threadID: string, question: string) => {
    try {
        const resp = await fetch(`${ environment.barfAssistant }/user-question`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                threadID,
                question
            })
        });

        const replies = await resp.json() as QuestionResponse[];

        return replies;

    } catch (error) {
        throw new Error('Error al crear Thread ID');
    }
}