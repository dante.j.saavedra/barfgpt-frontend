export interface ConversationsId{
    _id: string;
    username: string;
    sessionId: string;
}