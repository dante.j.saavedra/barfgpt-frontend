export * from './messages.interface';
export * from './ortographyResponse.interface';
export * from './questionResponse.interface';
export * from './conversationsId.interface';
export * from './conversation.interface';