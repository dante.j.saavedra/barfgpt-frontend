export interface Conversation{
    content: string;
    type: string;
}