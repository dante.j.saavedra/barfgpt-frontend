import { Routes } from '@angular/router';
import { DashboardLayoutComponent } from './presentation/layouts/dashboardLayout/dashboardLayout.component';
import { LoginLayoutComponent } from './presentation/layouts/loginLayout/loginLayout.component';
export const routes: Routes = [
  {
    path: 'assistant',
    component: DashboardLayoutComponent,
    children: [

      {
        path: 'assistant',
        loadComponent: () =>
          import('./presentation/pages/assistantPage/assistantPage.component'),
        data: {
          icon: 'fa-solid fa-user',
          title: 'Asistente',
          description: 'B.A.R.F. Assistant',
        },
      },
      
      {
        path: '**',
        redirectTo: 'assistant',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'login',
    component: LoginLayoutComponent,
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full',
  }
];