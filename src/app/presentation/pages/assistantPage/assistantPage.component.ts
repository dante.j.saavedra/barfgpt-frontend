import { CommonModule } from "@angular/common";
import { ChangeDetectionStrategy, Component, OnInit, inject, signal, effect } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { ChatMessageComponent, MyMessageComponent, TextMessageBoxComponent, TypingLoaderComponent } from "@components/components";
import { Conversation } from "@interfaces/conversation.interface";
import { Message } from "@interfaces/messages.interface";
import { BarfragService } from "@services/barfrag.service";
import { OpenAiService } from "@services/openai.service";

@Component({
    selector: 'app-assistant-page',
    standalone: true,
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ChatMessageComponent,
        MyMessageComponent,
        TypingLoaderComponent,
        TextMessageBoxComponent
    ],
    templateUrl: './assistantPage.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class AssistantPageComponent implements OnInit{
    public openaiService = inject(OpenAiService);
    public messages = signal<Message[]>([]);
    public isLoading = signal<boolean>(false);
    public threadID = signal<string|undefined>(undefined);

    public barfragService = inject(BarfragService);
    public conversation = signal<Conversation[]>([]);
    public ConversationId = this.barfragService.ConversationId$;


    
    constructor() {
      effect(() => {
        if (this.ConversationId!()) {
          this.loadMessages();
        }
    });

    }



    ngOnInit(): void {
        this.openaiService.createThread().subscribe(threadID => {
            this.threadID.set(threadID);
        });
        this.loadMessages()
    } 

    onConversationIdChange(newId: string): void {
      // Aquí puedes realizar la acción que desees cuando ConversationId cambia de valor
      console.log('ConversationId ha cambiado:', newId);
      // Por ejemplo, podrías recargar la conversación con el nuevo ID

    }

    loadMessages() {
      this.barfragService.conversationBySessionId(this.ConversationId!()).subscribe(conversation => {
        this.conversation.set(conversation);
    });
    }
    

    handleMessage2(question: string) {

        this.isLoading.set(true);
        this.conversation.update( prev => [...prev, { content: question, type: 'Human' }] );
    
        this.barfragService.MessageInConversationBySessionId( this.ConversationId!(), question ).subscribe( mensaje => {
            this.loadMessages();
            this.isLoading.set(false);
        });
          
    
    }






    handleMessage(question: string) {

        this.isLoading.set(true);
        this.messages.update( prev => [...prev, { text: question, isGpt: false }] );
    
        this.openaiService.postQuestion( this.threadID()!, question )
          .subscribe( replies => {
            const currentMessages = new Set(this.messages().map((msg) => msg.text));
            this.isLoading.set(false);
    
            for (const reply of replies) {
              for (const message of reply.content ) {
                if (!currentMessages.has(message)){
                  this.messages.update( prev => [
                    ...prev,
                    {
                      text: message,
                      isGpt: reply.role === 'assistant'
                    }
                  ]);
                }
    
              }
            }
    
    
    
          })
    
    }
}



