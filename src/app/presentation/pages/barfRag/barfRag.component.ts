import { CommonModule } from "@angular/common";
import { ChangeDetectionStrategy, Component, OnInit, inject, signal } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { ChatMessageComponent, MyMessageComponent, TextMessageBoxComponent, TypingLoaderComponent } from "@components/components";
import { Message } from "@interfaces/messages.interface";
import { OpenAiService } from "@services/openai.service";

@Component({
  selector: 'app-barf-rag',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ChatMessageComponent,
    MyMessageComponent,
    TypingLoaderComponent,
    TextMessageBoxComponent
],
  templateUrl: './barfRag.component.html',
  styleUrl: './barfRag.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BarfRagComponent implements OnInit{
  public openaiService = inject(OpenAiService);
  public messages = signal<Message[]>([]);
  public isLoading = signal<boolean>(false);

  public threadID = signal<string|undefined>(undefined);

  ngOnInit(): void {
      this.openaiService.createThread().subscribe(threadID => {
          this.threadID.set(threadID);
      });
  } 


  handleMessage(question: string) {

      this.isLoading.set(true);
      this.messages.update( prev => [...prev, { text: question, isGpt: false }] );
  
      this.openaiService.postQuestion( this.threadID()!, question )
        .subscribe( replies => {
          const currentMessages = new Set(this.messages().map((msg) => msg.text));
          this.isLoading.set(false);
  
          for (const reply of replies) {
            for (const message of reply.content ) {
              if (!currentMessages.has(message)){
                this.messages.update( prev => [
                  ...prev,
                  {
                    text: message,
                    isGpt: reply.role === 'assistant'
                  }
                ]);
              }
  
            }
          }
  
  
  
        })
  
  }
}