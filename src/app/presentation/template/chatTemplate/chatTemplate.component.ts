import { CommonModule } from "@angular/common";
import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { ChatMessageComponent, MyMessageComponent, TextMessageBoxComponent, TextMessageBoxEvent, TextMessageBoxFileComponent, TextMessageBoxSelectComponent, TextMessageEvent, TypingLoaderComponent } from "@components/components";
import { Message } from "@interfaces/messages.interface";
import { OpenAiService } from "@services/openai.service";

@Component({
    selector: 'app-chat-template',
    standalone: true,
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ChatMessageComponent,
        MyMessageComponent,
        TypingLoaderComponent,

        TextMessageBoxComponent,
        // TextMessageBoxFileComponent,
        // TextMessageBoxSelectComponent
    ],
    templateUrl: './chatTemplate.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatTemplateComponent {

    public openaiService = inject(OpenAiService);

    public messages = signal<Message[]>([]);
    public isLoading = signal<boolean>(false);
    
    handleMessage(prompt: string){
        console.log('prompt', prompt);
    }

    // handleMessageWithFile({prompt, file}: TextMessageEvent){
    //     console.log('prompt', prompt);
    //     console.log('file', file);
    // }

    // handleMessageWithSelect({prompt, selectedOption}: TextMessageBoxEvent){
    //     console.log('prompt', prompt);
    //     console.log('selectedOption', selectedOption);
    // }

}
