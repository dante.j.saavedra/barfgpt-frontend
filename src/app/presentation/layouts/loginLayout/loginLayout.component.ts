import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login-layout',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './loginLayout.component.html',
  styleUrl: './loginLayout.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginLayoutComponent {
  loginForm: FormGroup;
  


  constructor(private fb: FormBuilder, private router: Router) {
    this.loginForm = this.fb.group({
      username: ['']
    });
  }

  ngOnInit(): void {
    localStorage.clear();
  }

  onSubmit() {
    console.log('Nombre de usuario:', this.loginForm.value.username);
    localStorage.setItem('username', this.loginForm.value.username);

    // Redirigir a la página de asistente con router.navigate
    this.router.navigate(['/assistant']);
    console.log(this.router.url);

  }
}