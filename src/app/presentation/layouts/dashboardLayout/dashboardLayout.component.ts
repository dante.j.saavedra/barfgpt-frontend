import { CommonModule } from "@angular/common";
import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { RouterModule } from "@angular/router";
import { routes } from "../../../app.routes";
import { ConversationMenuItemComponent, SidebarMenuItemComponent } from "@components/components";
import { BarfragService } from "@services/barfrag.service";
import { ConversationsId } from "@interfaces/conversationsId.interface";


@Component({
    selector: 'app-dashboard-layout',
    standalone: true,
    imports: [
        CommonModule,
        RouterModule,
        SidebarMenuItemComponent,
        ConversationMenuItemComponent

    ],
    templateUrl: './dashboardLayout.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardLayoutComponent {
    public barfragService = inject(BarfragService);
    public username = localStorage.getItem('username');
    public routes = routes[0].children?.filter( (route) => route.data );
    public conversaciones = signal<ConversationsId[]>([]);

    ngOnInit(): void {
        this.obtenerConversaciones()
    }

    nuevaConversacion() {
        this.barfragService.newConversationByUsername(this.username!).subscribe( sessionId => {
            this.obtenerConversaciones()
        });
    }

    obtenerConversaciones() {
        this.barfragService.conversationsByUsername(this.username!).subscribe( conversations => {
            this.conversaciones.set(conversations)
        });
    }
}

    

