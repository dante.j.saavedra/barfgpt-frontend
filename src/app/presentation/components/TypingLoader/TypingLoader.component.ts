import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-typing-loader',
    standalone: true,
    templateUrl: './TypingLoader.component.html',
    styleUrl: './TypingLoader.component.css',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypingLoaderComponent { }
