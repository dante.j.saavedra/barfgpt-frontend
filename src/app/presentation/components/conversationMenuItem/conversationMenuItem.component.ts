import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, inject } from '@angular/core';
import { BarfragService } from '@services/barfrag.service';

@Component({
  selector: 'app-conversation-menu-item',
  standalone: true,
  imports: [
    CommonModule,
  ],
  templateUrl: './conversationMenuItem.component.html',
  styleUrl: './conversationMenuItem.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConversationMenuItemComponent {
  public barfragService = inject(BarfragService);

  @Input({required: true}) id!: string;


    selectConversation() {
        console.log('Seleccionar conversación:', this.id);
        localStorage.setItem('ConversationId', this.id);
        this.barfragService.ConversationId$.set(this.id);
    }
 }
