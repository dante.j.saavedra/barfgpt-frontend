import { Injectable } from '@angular/core';
import { createThreadUseCase, postQuestionUseCase } from '@use-cases/use-cases';
import { Observable, from, of, tap } from 'rxjs';

@Injectable({providedIn: 'root'})
export class OpenAiService {
    
    createThread():Observable<string>{
        if (localStorage.getItem('threadID')) {
           return of (localStorage.getItem('threadID')!);
        }
        return from (createThreadUseCase()).pipe(
            tap((threadID) => {
                localStorage.setItem('threadID', threadID);
            })
        );
    }

    postQuestion(threadID: string, question: string){
        return from(postQuestionUseCase(threadID, question));
    }


}