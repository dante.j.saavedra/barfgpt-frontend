import { Injectable, signal } from '@angular/core';
import { Conversation, ConversationsId } from '@interfaces/interfaces';
import { getConversationsByUsername, newConversationsByUsername,getMessagesBySessionId, addMessageInConversationBySessionId } from '@use-cases/use-cases';
import { Observable, from, of, tap } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class BarfragService {

  ConversationId$ = signal<string>('');





  conversationsByUsername(username: string): Observable<ConversationsId[]> {
    return from(getConversationsByUsername(username)).pipe(
      tap((conversations) => {
        localStorage.setItem(
          'ConversationsByUsername',
          JSON.stringify(conversations)
        );
      })
    );
  }

  newConversationByUsername(username: string): Observable<string> {
    return from(newConversationsByUsername(username));
  }

  conversationBySessionId(sessionId: string): Observable<Conversation[]> {
    return from(getMessagesBySessionId(sessionId)).pipe(
      tap((conversations) => {
        localStorage.setItem(
          'ConversationsByUsername',
          JSON.stringify(conversations)
        );
      })
    );
  }
  MessageInConversationBySessionId(sessionId: string, question: string): Observable<string> {
    return from(addMessageInConversationBySessionId(sessionId, question));
  }

}
