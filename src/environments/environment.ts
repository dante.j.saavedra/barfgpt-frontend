export const environment = {
    backendApi: 'http://barfia.datafusion.cl:3000/gpt',
    barfAssistant: 'http://barfia.datafusion.cl:3000/barf-assistant',
    barfRag: 'http://barfia.datafusion.cl:3000/barf-rag',
    // backendApi: 'http://localhost:3000/gpt',
    // barfAssistant: 'http://localhost:3000/barf-assistant',
    // barfRag: 'http://localhost:3000/barf-rag',
};
